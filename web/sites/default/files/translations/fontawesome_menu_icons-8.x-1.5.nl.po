# Dutch translation of Font Awesome Menu Icons (8.x-1.5)
# Copyright (c) 2018 by the Dutch translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Font Awesome Menu Icons (8.x-1.5)\n"
"POT-Creation-Date: 2018-11-28 17:46+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Dutch\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Cancel"
msgstr "Annuleren"
msgid "Settings"
msgstr "Instellingen"
msgid "Menu"
msgstr "Menu"
msgid "Accept"
msgstr "Accepteren"
msgid "About"
msgstr "Over"
