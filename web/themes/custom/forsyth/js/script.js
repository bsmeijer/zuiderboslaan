(function ($) {
	'use strict';

	Drupal.behaviors.borrowCarousel = {
		attach: function (context, settings) {
			$('.node--type-borrow-item.node--view-mode-full .field--name-field-images').once().slick({
				arrows: true,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				centerMode: true,
			});
		}
	};

	Drupal.behaviors.expandImage = {
		attach: function (context, settings) {
			$('.photo-album-photos .field', context).click(function () {

				var $this = $(this);
				$('.photo-album-photos .reference-info-window').slideUp(300, function () {
					$(this).remove();
				});

				if ($this.hasClass('reference-window-active')) {
					$this.removeClass('reference-window-active');
				}
				else {
					var index = $('.photo-album-photos .field').index(this),
						insert_index = calculate_insert_index(index, $('.photo-album-photos .field').length);

					$('.photo-album-photos .field').removeClass('reference-window-active');
					$this.addClass('reference-window-active');

					var src = $this.find('img').attr('src');
					let newSrc = src.replace('/styles/square_big/public', '');

					let image = $this.find('img').clone();
					image.attr('src', newSrc);
					image.attr('width', '');
					image.attr('height', '');

					image.insertAfter('.photo-album-photos .field:eq(' + insert_index + ')')
						.wrap('<div class="reference-info-window" style="display:none"></div>')
						.parent()
						.slideDown();
				}

				function calculate_insert_index(index, nr_items) {
					var window_width = $(window).width(),
						items_row = 2,
						insert_index;

					// Magic numbers correspond with theme breakpoints in ems.
					if (window_width >= 1280) {
						items_row = 5;
					}
					else if (window_width >= 1024) {
						items_row = 4;
					}
					else if (window_width >= 768) {
						items_row = 3;
					}

					insert_index = ((Math.floor(index / items_row) + 1) * items_row) - 1;

					return (insert_index > nr_items) ? (nr_items - 1) : insert_index;
				}
			});
		}
	};

	Drupal.behaviors.commentLinks = {
		attach: function (context, settings) {
			$('.expand-links').click(function (e) {
				$('.comment-actions ul').removeClass('expanded');
				$(this).siblings('.dropdown').addClass('expanded');
				e.preventDefault();
			});
		}
	};

	Drupal.behaviors.imageUpload = {
		attach: function (context, settings) {
			$('.block-formblock-node', context).once('field-post-image-add').on('click', '#post-photo-add', function (e) {
				$('input[data-drupal-selector="edit-field-image-0-upload"]').trigger('click');
				e.preventDefault();
			});

			$('.block-formblock-node', context).once('field-post-image-remove').on('click', '#post-photo-remove', function (e) {
				$('input[data-drupal-selector="edit-field-image-0-remove-button"]').trigger('mousedown');
				e.preventDefault();
			});
		}
	};

	Drupal.behaviors.mobile_menu = {
		attach: function (context, settings) {
			// Add the wrapper and the blinder to the main content.
			$('body').once().append('<div class="mobile-menu-blinder"></div><div class="mobile-menu-wrapper"></div>');

			// Find the main-menu and put it in the wrapper.
			let mainmenu = $('.menu--main > ul.menu').once().clone();
			mainmenu.addClass('main-menu');
			$('.mobile-menu-wrapper').append(mainmenu);

			// Second verse, same as the first, but now for the account-menu.
			// Added the selector: .region header to not copy the menu (on the user pages twice).
			let usermenu = $('.region-header .menu--account > ul.menu').once().clone();
			usermenu.addClass('account-menu');
			$('.mobile-menu-wrapper').append(usermenu);

			// Add the menu-toggle behaviour
			$('.menu-toggler a').click(function (e) {
				$('body').addClass('mobile-menu-open');
				e.preventDefault();
			});

			// Add the searchbar behaviour
			$('.search-toggler a').click(function (e) {
				if ($('body').hasClass('search-bar-open')) {
					$('body').removeClass('search-bar-open');
					e.preventDefault();
					return;
				}
				$('body').addClass('search-bar-open');
				e.preventDefault();
			});

			// Add the closing behaviour.
			$('.mobile-menu-blinder').click(function () {
				$('body').removeClass('mobile-menu-open');
				$('body').removeClass('search-bar-open');
			});
		}
	};
})(jQuery);
